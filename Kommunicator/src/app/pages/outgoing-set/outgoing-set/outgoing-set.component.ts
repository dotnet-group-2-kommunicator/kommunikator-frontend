import { Component, Input, OnInit } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { env } from 'process';
import { MessageService } from 'src/app/services/create-message/message.service';
import { EmailService } from 'src/app/services/email/email.service';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';
import { TopicService } from 'src/app/services/topic/topic.service';
import { environment } from 'src/environments/environment';
import { EmailMessage } from 'src/models/emailMessage.model';
import { Message } from 'src/models/message';
import { MessageTemplate } from 'src/models/MessageTemplate';
import { responseTemplate } from 'src/models/responseTemplate';
import { Topic } from 'src/models/topic.model';
import { Category } from '../../../../models/category.model';
import { CategoryService } from '../../../services/category/category.service';

@Component({
  selector: 'app-outgoing-set',
  templateUrl: './outgoing-set.component.html',
  styleUrls: ['./outgoing-set.component.scss']
})

export class OutgoingSetComponent implements OnInit {
  steps: any[] = [
    {
      name: "Introduction",
    },
    {
      name: "Select Topics",
      modalTitle: "No Topics Selected",
      error: "Please select at least one subscribed topic you wish to use in the outgoing set",
      method: "Step1"
    },
    {
      name: "Select a Message",
      modalTitle: "No Message Selected",
      error: "Please select a message you wish to use for this outgoing set",
      method: "Step2"
    },
    {
      name: "Review",
      button: "Deliver Outgoing Set",
      modalTitle: "Outgoing Set Delivered",
      error: "Your messages have now been delivered, you will be redirected to the homepage shortly",
      method: "Deliver"
    }
  ];

  activeStep: number = 1;

  topics: Topic[];
  selectedTopics: Topic[];

  showModal: boolean = false;

  API_KEY_MCE: string = environment.API_KEY_TINYMCE;

  // Dynamic Variables
  subscriberEmails: string[] = [];
  messages: Message[] = [];

  selectedMessage: Message = null;


  constructor(
    private topicService: TopicService,
    private messageService: MessageService,
    private emailService: EmailService,
    private messageTemplateService: MessageTemplateService,
    private responseTemplateService: ResponseTemplateService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.topicService.fetchAllTopics().subscribe((res) => {
      let subscribedTopics: Topic[] = res.filter(t => t.topicSubscriptions.length > 0);
      this.topics = subscribedTopics;

      let emails = this.topics.map(t => t.topicSubscriptions);
    })
  }

  get StepCrumbs() : string[] {
    let names = this.steps.filter(step => step.name).map(step => step.name);
    return names;
  }

  get Step() : any {
    return this.steps[this.activeStep - 1];
  }

  public async Advance() {
    if ((this.activeStep + 1) > this.steps.length) {
      return;
    }

    let step: any = this.Step;

    if (typeof step.method === "string") {
      // If the return is true, then the step has the required data
      let result: boolean = await this[step.method]();

      if (result !== true) {
        //alert(step.error);
        this.showModal = true;
        return;
      }

      this.activeStep += 1;
      return;
    }

    this.activeStep += 1;
  }

  public Retreat() {
    if (this.activeStep - 1 < 1) {
      return;
    }

    this.activeStep -= 1;
  }

  private counter(i: number) {
    return new Array(i);
  }

  // Methods for each step
  CheckStep(methodName: string) {

  }

  async Step1() {
    if (this.subscriberEmails.length > 0) {
      this.messageService.fetchAllMessages().subscribe(res => {
        this.messages = res;
      });
      return true;
    }

    return false;
  }

  Step2() {
    return this.selectedMessage != null;
  }

  CreateResponseURLs(message: Message) {
    let uniqueURL: string[] = [];

    this.subscriberEmails.forEach(email => {
      let urldata = {
        responseTemplateId: message["responseTemplateId"], // Having the last 'd' capitalized didn't work, so w/e
        userEmail: email,
        messageId: message.messageID
      }

      let data = btoa(JSON.stringify(urldata));

      //let url: string = `http://localhost:4200/${this.selectedMessage.responseTemplate.responseType == 0 ? "text-response-page" : "likert-response-page"}/${data}`;
      let url: string = `https://kommunicator.herokuapp.com/${this.selectedMessage.responseTemplate.responseType == 0 ? "text-response-page" : "likert-response-page"}/${data}`;

      uniqueURL.push(url);
    });

    return uniqueURL;
  }

  Deliver() {
    let uniqueURL: string[] = this.CreateResponseURLs(this.selectedMessage);

    this.selectedMessage.messageTemplate.content += `<br/><a href="-responseurl-">Click here to respond to the email</a>`;

    let email: EmailMessage = {
      Emails: this.subscriberEmails,
      UniqueLinks: uniqueURL,
      Topics: this.selectedTopics,
      Subject: `Kommunicator Newsletter '${this.selectedMessage.messageDescription}'`,
      Message: this.selectedMessage
    }

    this.emailService.deliverEmail(email).subscribe(res => {
      this.ReturnToMenu();
    });

    return false;
  }

  ReturnToMenu() {
    this.showModal = true;
    setTimeout(_ => {
      this.router.navigateByUrl('/admin-main-page');
    }, 2000);
  }

  // Modal events
  CloseModal() {
    this.showModal = false;
  }

  // Step 1: Item selected
  onTopicsSelected(topics) {
    this.selectedTopics = topics;
  }

  // Step 2: EMAIL
  UpdateSubscriptionList(emails) {
    this.subscriberEmails = emails;
  }

  // Step 3: Select a message
  SelectMessage(message) {
    this.selectedMessage = message;

    // Retrieve the templates
  }
}
