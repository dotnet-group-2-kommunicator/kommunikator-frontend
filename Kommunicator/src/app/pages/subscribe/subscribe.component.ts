import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EmailService } from 'src/app/services/email/email.service';
import { filter, map } from 'rxjs/operators';
import { TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnInit {

  public API_KEY_MCE=environment.API_KEY_TINYMCE;

  listOfAdministrators:any[];
  dataModel:any[];

  constructor(private emailService:EmailService) { }
  

  signUp(x){
    this.emailService.getUsers()
    .subscribe(
      users=>{
       this.emailService.addedSubscribers=[...users];
       console.log(this.emailService.addedSubscribers);
      },
      err=>{
          console.log(err)
      }
    
    );
  }
  SendEmailToUsers(){
    this.emailService.sendEmailToMultipleUsers().subscribe(
      users=>{
        console.log("Sucsessfully sent emails");
      },
      errors=>{
        console.log("Something went wrong");
      }
    );

  }
  getDataFromTinyMce(){
    console.log(this.dataModel);
  }

  ngOnInit(): void {
    this.emailService.sendEmailToMultipleUsers();
  }

}
