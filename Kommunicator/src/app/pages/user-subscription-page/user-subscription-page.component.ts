import { Component, OnInit } from '@angular/core';
import { Category } from 'src/models/category.model';
import { CategoryService } from 'src/app/services/category/category.service';
import { TopicService } from 'src/app/services/topic/topic.service';

import { Topic } from 'src/models/topic.model';
import { TopicSubscription, TopicSubscriptions } from 'src/models/TopicSubscription.model';
import { TopicSubscriptionService } from 'src/app/services/topic-subscription/topic-subscription.service';
import { HttpHeaders } from '@angular/common/http';
import { EmailService } from 'src/app/services/email/email.service';

@Component({
  selector: 'app-user-subscription-page',
  templateUrl: './user-subscription-page.component.html',
  styleUrls: ['./user-subscription-page.component.scss']
})

export class UserSubscriptionPageComponent implements OnInit {

  categories: Category[] = [];
  topicSubscription: TopicSubscriptions;
  /*personalTopicSubscriptions = {
    topicSubscriptions: []
  } as TopicSubscrition;*/

  login: string = "sven4696@gmail.com"

  searchText;
  categorySearch;
  subCategorySearch;
  message: string;
  allSubscriptions;

  constructor(private emailService: EmailService, 
    private categoryService: CategoryService, 
    private topicService: TopicService, 
    private subscriptionService: TopicSubscriptionService) { }

  ngOnInit(): void {
    this.emailService.currentUserModel.subscribe(res => {
      this.login = res;
    });

    this.categoryService.fetchAllCategories().subscribe((res) => {
      this.categories = res;   
      console.log(this.categories)
    })

    this.subscriptionService.fetchAllSubscriptions(this.login).subscribe((res) => {
      this.topicSubscription = res;
      console.log("SUBS", res);
    })
  }

  receiveMessage($event) {
    this.subCategorySearch = $event
  }

  receiveAllTopics($event) {
    this.categorySearch = ''
    this.subCategorySearch = ''
    this.searchText = ''
  }

  fetchUpdatedSubscriptions() {
    this.categoryService.fetchAllCategories().subscribe((res) => {
      this.categories = res;   
      console.log(this.categories)
    })
  }

  checkSubscribed(topic: Topic): boolean{
    if (this.topicSubscription === undefined) return true;
    
    let subscriptions = topic.topicSubscriptions.filter(ts => ts.active === true && ts.userEmail === this.topicSubscription.userEmail);

    return subscriptions.length > 0;
  }

  SubscribeToTopic(topic) {
    if (this.checkSubscribed(topic))
    {
      this.subscriptionService.updateActiveFlag(topic.id, this.login).subscribe((user) => {
        this.fetchUpdatedSubscriptions();
      });
    } else {
      this.subscriptionService.subscribeTopic(topic.id, this.login).subscribe((res) => {
        this.fetchUpdatedSubscriptions();
      })
    }
    /*console.log('this is the topic: ', topic)
    var topicSubscription = topic.topicSubscriptions
    if(topicSubscription.length == 0 || topicSubscription == undefined) {
      this.subscriptionService.subscribeTopic(topic.id, this.login).subscribe((res) => {
        console.log("New Sub", res)
        this.fetchUpdatedSubscriptions();
      })
    }
    else {
      topic.topicSubscriptions.forEach(element => {
        if (element == undefined || element.userEmail !== this.login) {
          this.subscriptionService.subscribeTopic(topic.id, this.login).subscribe((res) => {
            console.log("Resub", res)
            this.fetchUpdatedSubscriptions();
          })
        }
        else if(element.userEmail == this.login && (element.active == true || element.active == false)) {
          this.subscriptionService.updateActiveFlag(topic.id, this.login).subscribe((user) => {
            this.fetchUpdatedSubscriptions();
          });
        }
      });
    }*/
  }
}
