import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateResponseTemplateComponent } from './create-response-template.component';

describe('CreateResponseTemplateComponent', () => {
  let component: CreateResponseTemplateComponent;
  let fixture: ComponentFixture<CreateResponseTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateResponseTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateResponseTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
