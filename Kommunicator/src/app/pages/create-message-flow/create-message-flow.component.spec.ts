import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMessageFlowComponent } from './create-message-flow.component';

describe('CreateMessageFlowComponent', () => {
  let component: CreateMessageFlowComponent;
  let fixture: ComponentFixture<CreateMessageFlowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMessageFlowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMessageFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
