import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { env } from 'process';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { TopicService } from 'src/app/services/topic/topic.service';
import { environment } from 'src/environments/environment';
import { Topic } from 'src/models/topic.model';
import { Category } from '../../../models/category.model';
import { CategoryService } from '../../services/category/category.service';
import { responseTemplate } from '../../../models/responseTemplate';
import { MessageTemplate } from '../../../models/MessageTemplate';
import { CreateMessageTemplateComponent } from 'src/app/components/create-message-template/create-message-template.component';
import { CreateResponseTemplateComponent } from '../create-response-template/create-response-template.component';
import { Message } from 'src/models/message';
import { MessageService } from 'src/app/services/create-message/message.service';

@Component({
  selector: 'app-create-message-flow',
  templateUrl: './create-message-flow.component.html',
  styleUrls: ['./create-message-flow.component.scss']
})
export class CreateMessageFlowComponent implements OnInit {
  steps: any[] = [
    {
      name: "Introduction",
      modalTitle: "No Message Description Specified",
      error: "Please enter a message description you wish to display",
      method: "CheckDescription",
    },

    {
      name: "Select Message Template",
      modalTitle: "No Message Template Selected",
      error: "Please select a message template you wish to use for this outgoing set",
      //method: "Step1"
      method: "CheckTemplateId",
    },
    {
      name: "Select Response Template",
      modalTitle: "No Response Template Selected",
      error: "Please check that you've selected a response template and a response type",
      onDisplay: "OnReview",
      method: "CheckResponseId",
    },

    {
      name: "Review",
      modalTitle: "Message Created",
      error: "Your messages have now been delivered, you will be redirected to the homepage shortly",
      button: "Create Message",
      method: "CreateMessage"
      //check: this.Step3
    },
  ];

  activeStep: number = 1;

  topics: Topic[];
  selectedTopics: Topic[];

  showModal: boolean = false;

  API_KEY_MCE: string = environment.API_KEY_TINYMCE;

  textContent: string = `
  <h1>H1</h1>
  <h2>H1</h2>
  <h3>H3</h3>
  <h4>H4</h4>
  <h5>H5</h5>
  <h6>H6</h6>
  `;

  // Child Components
  @ViewChildren("msgTemplate") messageTemplateChildren: QueryList<CreateMessageTemplateComponent>;
  @ViewChild(CreateResponseTemplateComponent, { static: false }) responseTemplateChild: CreateResponseTemplateComponent;

  messageTemplateChild: CreateMessageTemplateComponent;

  // Dynamic Variables
  messageDescription: string = null;
  messageTemplateId: number = -1;
  responseTemplateId: number = -1;

  // Retrieved Review Data
  responseType: string = null;
  responseTemplate: responseTemplate = null;
  messageTemplate: MessageTemplate = null;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private topicService: TopicService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.topicService.fetchAllTopics().subscribe((res) => {
      let subscribedTopics: Topic[] = res.filter(t => t.topicSubscriptions.length > 0);
      this.topics = subscribedTopics;
    })
  }

  ngAfterViewInit() : void {
    this.messageTemplateChildren.changes.subscribe((comps: QueryList<CreateMessageTemplateComponent>) => {
      if (typeof comps.first !== 'undefined'){
        this.messageTemplateChild = comps.first;
      }
    });
  }

  get StepCrumbs() : string[] {
    let names = this.steps.filter(step => step.name).map(step => step.name);
    return names;
  }

  get Step() : any {
    return this.steps[this.activeStep - 1];
  }

  public async Advance() {
    let step: any = this.Step;

    if ((this.activeStep + 1) > this.steps.length) {
      await this[step.method]();
      return;
    }

    // Execute some logic just before the next page is displayed

    if (typeof step.method === "string") {
      // If the return is true, then the step has the required data
      let result: boolean = await this[step.method]();

      if (result !== true) {
        this.showModal = true;
        return;
      }

      if (typeof step.onDisplay === "string") {
        await this[step.onDisplay]();
      }

      this.activeStep += 1;
      return;
    }

    this.activeStep += 1;
  }

  public async Retreat() {
    if (this.activeStep - 1 < 1) {
      return;
    }

    this.activeStep -= 1;
    let step: any = this.Step;

    if (typeof step.onBack === "string") {
      await this[step.onBack]();
    }
  }

  private counter(i: number) {
    return new Array(i);
  }

  // Methods for each step
  CheckStep(methodName: string) {

  }

  Step1() {
    return false;
  }

  CheckDescription() {
    console.log(this.Step.error);
    return this.messageDescription !== null && this.messageDescription.length > 0;
  }

  CreateMessage() {
    let message: Message = {
      responseTemplateID: this.responseTemplateId,
      messageTemplateID: this.messageTemplateId,
      messageDescription: this.messageDescription
    };

    this.messageService.createMessage(message).subscribe(res => {
      this.router.navigateByUrl('/admin-main-page')
    })
  }

  async CheckTemplateId() {
    if (this.messageTemplateId !== -1) {
      await this.messageTemplateChild.saveMessageTemplate();
      return true;
    }
    
    return false;
  }

  async CheckResponseId() {
    //return this.responseTemplateId !== -1;
    if (this.responseTemplateId !== -1) {
      await this.responseTemplateChild.saveResponseTemplate();
      return true;
    }

    if (this.responseType !== null) {
      return true;
    }

    return false;
  }

  OnReview() {
    // 2. Get Message Template (based on Id)
    this.http.get(`https://kommunicator.azurewebsites.net/api/MessageTemplates/${this.messageTemplateId}`).subscribe(res => {
      this.messageTemplate = res;
    })
    // 3. Get Response Template (based on Id)
    this.http.get(`https://kommunicator.azurewebsites.net/api/ResponseTemplates/${this.responseTemplateId}`).subscribe(res => {
      this.responseTemplate = res;
    })
  }

  ReturnToMenu() {
    //this.router.navigateByUrl('/admin-main-page');
    this.showModal = true;
  }

  // Modal events
  CloseModal() {
    this.showModal = false;
  }

  OnDescriptionChange(description) {
    this.messageDescription = description;
  }

  OnMessageIdFetch(event) {
    this.messageTemplateId = event;
  }

  OnReponseIdFetch(event) {
    this.responseTemplateId = event;
  }

  OnResponseTypePicked(event) {
    this.responseType = event;
  }
}
