import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-log-in-page',
  templateUrl: './log-in-page.component.html',
  styleUrls: ['./log-in-page.component.scss']
})
export class LogInPageComponent implements OnInit {
  mytoken: string = "as";
  a: any = "s";

  constructor(private auth: AuthenticationService, private bs: BackendService) {
    this.auth.GetToken()
    .then(token => {
      console.log("Test");
      this.mytoken = token;
      //console.log(this.mytoken)
    })

    this.bs.get("test/auth")
    .subscribe(a => {
      console.log(a);
    })
  }

  ngOnInit(): void {
  }

}
