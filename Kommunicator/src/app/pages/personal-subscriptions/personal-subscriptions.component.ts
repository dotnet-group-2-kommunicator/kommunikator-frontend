import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmailService } from 'src/app/services/email/email.service';
import { TopicSubscriptionService } from 'src/app/services/topic-subscription/topic-subscription.service';
import { TopicSubscription, TopicSubscriptions } from 'src/models/TopicSubscription.model';

@Component({
  selector: 'app-personal-subscriptions',
  templateUrl: './personal-subscriptions.component.html',
  styleUrls: ['./personal-subscriptions.component.scss']
})
export class PersonalSubscriptionsComponent implements OnInit {

  login: string = "sven4696@gmail.com"
  topicSubscription = {
    topicSubscriptions: []
  } as TopicSubscriptions; 

  searchText;
  categorySearch ;
  subCategorySearch;
  message: string;
  isChecked = true;
  

  constructor(private subscriptions: TopicSubscriptionService, private emailService: EmailService) { }

  ngOnInit(): void {
    this.emailService.currentUserModel.subscribe(res => {
      console.log("Email", res);
      this.login = res;
    })
    
    this.subscriptions.fetchAllSubscriptions(this.login).subscribe((res: TopicSubscriptions) => {
      this.topicSubscription = res;
      console.log(this.topicSubscription)
    })
  }
  
  get topicsSubscribedTo() {
    return this.topicSubscription.topicSubscriptions.filter(topic => {
      return topic.active
    })
  }

  
  get topicsNotSubscribedTo() {
    return this.topicSubscription.topicSubscriptions.filter(topic => {
      return !topic.active
    })
  }

  onTopicSubscriptionChanged(topicId) {
    this.subscriptions.updateActiveFlag(topicId, this.login).subscribe((user) => {
      console.log('working')
     });

    for (let i = 0; i < this.topicSubscription.topicSubscriptions.length; i++) {      
      if (this.topicSubscription.topicSubscriptions[i].topicID === topicId) {
        this.topicSubscription.topicSubscriptions[i].active = !this.topicSubscription.topicSubscriptions[i].active;
        break;
      }
    }
  }

  unSubscribeAll() {  
    for(let i =0; i< this.topicSubscription.topicSubscriptions.length; i++){  
      if (this.topicSubscription.topicSubscriptions[i].active === true) {
        this.topicSubscription.topicSubscriptions[i].active = false;
        this.subscriptions.updateActiveFlag(this.topicSubscription.topicSubscriptions[i].topicID, this.login).subscribe((user) => {
          console.log("unsubscribed")
        })
      }
    }
    console.log(this.topicSubscription)
  }

 

}
