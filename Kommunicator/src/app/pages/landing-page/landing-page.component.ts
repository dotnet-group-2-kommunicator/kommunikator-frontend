import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { EmailService } from 'src/app/services/email/email.service';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  images = [0, 119, 180].map((n) => `https://picsum.photos/id/${n}/900/400`);

  email: NgModel;

  getEmail(x){
    this.email = x;
  }
  constructor(private emailService: EmailService, private router: Router) { }

  userSubscription: Subscription;

  ngOnInit(): void {
  }

  UserSubscribe() {
    let email = this.email.viewModel;
    //this.email.subsc
    this.emailService.changeLoginEmail(email);

    // Check user existence
    this.emailService.doesUserExist(email).subscribe(state => {
      if (state === true) {
        this.router.navigateByUrl("/user-subscription");
      } else {
        this.emailService.registerUser(email).subscribe(user => {
          this.router.navigateByUrl("/user-subscription");
        })
      }
    });
  }
}
