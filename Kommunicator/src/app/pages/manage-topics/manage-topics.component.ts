import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category/category.service';
import { SubCategoryService } from 'src/app/services/sub-category/sub-category.service';
import { Category } from 'src/models/category.model';
import { SubCategory } from 'src/models/subcategory.model';
import { Topic } from 'src/models/topic.model';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CategorySidebarComponent} from 'src/app/components/category-sidebar/category-sidebar.component';
import { EditTopicFormComponent } from 'src/app/components/edit-topic-form/edit-topic-form.component';
import { TopicService } from 'src/app/services/topic/topic.service';

export interface TopicFormData {
  categories: Category[],
  topic: Topic,
  subCategory: SubCategory
};

@Component({
  selector: 'app-manage-topics',
  templateUrl: './manage-topics.component.html',
  styleUrls: ['./manage-topics.component.scss']
})
export class ManageTopicsComponent implements OnInit {

  categories: Category[];

  activeCategory: Category;

  loading: boolean;

  activeSubCategory: SubCategory = {
    name: "",
    categoryID: -1
  };

  subCategorySearch: string = "";

  activeTopic: Topic = {
    subCategoryID: -1,
    name: "",
    description: ""
  };

  constructor(private categoryService: CategoryService, 
    private subCategoryService: SubCategoryService,
    private topicService: TopicService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.loading = true;
    this.categoryService.fetchAllCategories().subscribe((res) => {
      if(res) {
        this.loading = false;
      }
      this.categories = res;
    });
  }

  handleAllTopic() {
    this.subCategorySearch = "";
  }

  hideLoader() {
    document.getElementById('loading').style.display = 'none';
  }

  openNewTopicForm() {
    const topicFormData = {
      categories: this.categories,
      topic: this.activeTopic,
      subCategory: this.activeSubCategory
    };
    const dialogRef = this.dialog.open(EditTopicFormComponent, {
      width: '50vw',
      data: topicFormData
    });

    dialogRef.afterClosed().subscribe(formResult => {
      if (formResult != undefined && formResult != null) {
        const topic = formResult.topic;
        const subCat = formResult.subCategory;
        topic.subCategoryID = formResult.subCategory.id;
        topic.active = true;

        this.topicService.addTopic(topic).subscribe(serviceResult => {
          
          this.categories.map((c, cIndex) => {
            if (c.id == subCat.categoryID){
              c.subCategories.map((sc, scIndex) => {
                if (sc.id == serviceResult.subCategoryID) {
                  this.categories[cIndex].subCategories[scIndex].topics.push(serviceResult);
                  console.log(this.categories[cIndex].subCategories[scIndex].topics);
                  this.subCategorySearch = sc.name;
                }
              });
            }
          });
        })
      }
    });
  }
  
  receiveMessage(name: string) {
    this.subCategorySearch = name;
  }

  handleNewCategoryEvent(category: Category) {
    if (category == null) {
      return;
    }

    this.categoryService.newCategory(category).subscribe((res) => {
      res.subCategories = [];
      this.categories.push(res);
    });

  }

  handleNewSubCategoryEvent(subCategory: SubCategory) {
    if (subCategory == null) {
      return;
    }

    this.subCategoryService.newSubCategory(subCategory).subscribe((res) => {
      res.topics = [];
      this.categories.map(c => {
        if (c.subCategories == null) {
          c.subCategories = [];
        }
        if (c.id == subCategory.categoryID) {
          c.subCategories.push(res);
          return;
        }
      });
    });
  }

  handleEditCategoryEvent(category: Category) {
    if (category == null) {
      return;
    }

    this.categoryService.editCategory(category).subscribe((res) => {
      this.categories.map(c => {
        if (c.id == res.id) {
          c.name = res.name;
          return
        }
      })
    });

  }

  handleEditSubCategoryEvent(subCategory: SubCategory) {
    if (subCategory == null) {
      return;
    }
    console.log(subCategory);
    this.subCategoryService.editSubCategory(subCategory).subscribe((res) => {
      this.categories.map((c, cIndex) => {
        if (c.id == subCategory.categoryID) {
          
          c.subCategories.map((sc, scIndex) => {
            if (sc.id == res.id) {
              this.categories[cIndex].subCategories[scIndex].name = res.name;
            }
          });
        }
      });
    });
  }

  handleEditTopicEvent(eventData: any) {
    if (eventData == null) {
      return;
    }

    const topicFormData = {
      categories: this.categories,
      topic: eventData.topic,
      subCategory: eventData.subCategory
    };
    const dialogRef = this.dialog.open(EditTopicFormComponent, {
      width: '50vw',
      data: topicFormData
    });

    dialogRef.afterClosed().subscribe(formResult => {
      if (formResult != undefined && formResult != null) {
        const topic = formResult.topic;
        topic.subCategoryID = formResult.subCategory.id;
        topic.active = true;

        this.topicService.editTopic(topic).subscribe(serviceResult => {
          const subCat = formResult.subCategory;
          this.categories.map((c, cIndex) => {
            if (c.id == subCat.categoryID){
              c.subCategories.map((sc, scIndex) => {
                if (sc.id == serviceResult.subCategoryID) {
                  sc.topics.map((t, tIndex) => {
                    if (t.id == serviceResult.id) {
                      this.categories[cIndex].subCategories[scIndex].topics[tIndex] = serviceResult;
                    }
                  })
                }
              });
            }
          });
        })
      }
    });

    this.topicService.editTopic(eventData.topic).subscribe((result) => {
      
    });
  }

  handleDeleteTopicEvent(eventData: any) {
    console.log(eventData)
    if (eventData == null) {
      return;
    }
    this.topicService.deleteTopic(eventData.topic).subscribe((res) => {
      this.categories.map((c, cIndex) => {
        if (c.id == eventData.subCategory.categoryID){
          c.subCategories.map((sc, scIndex) => {
            if (sc.id == res.subCategoryID) {
              sc.topics.map((t, tIndex) => {
                if (t.id == res.id) {
                  this.categories[cIndex].subCategories[scIndex].topics.splice(tIndex, 1);
                }
              })
            }
          });
        }
      });
    });
  }

  handleDeleteCategoryEvent(categoryId: number) {
    if (categoryId == null) {
      return;
    }
    const categoriesCopy = this.categories;
    this.categoryService.deleteCategory(categoryId).subscribe(() => {
      categoriesCopy.map((c, index) => {
        if (c.id == categoryId) {
          this.categories.splice(index, 1);
        }
      })
    });

  }

  handleDeleteSubCategoryEvent(subCategory: SubCategory) {
    if (subCategory == null) {
      return;
    }
    const categoriesCopy = this.categories;
    this.subCategoryService.deleteSubCategory(subCategory).subscribe(() => {
      categoriesCopy.map((c, cIndex) => {
        if (c.id == subCategory.categoryID) {
          c.subCategories.map((sc, scIndex) => {
            if (sc.id == subCategory.id) {
              this.categories[cIndex].subCategories.splice(scIndex, 1);
            }
          })
        }
      })
    });
  }
}