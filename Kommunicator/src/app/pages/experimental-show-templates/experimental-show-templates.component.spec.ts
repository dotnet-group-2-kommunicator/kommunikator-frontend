import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperimentalShowTemplatesComponent } from './experimental-show-templates.component';

describe('ExperimentalShowTemplatesComponent', () => {
  let component: ExperimentalShowTemplatesComponent;
  let fixture: ComponentFixture<ExperimentalShowTemplatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExperimentalShowTemplatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperimentalShowTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
