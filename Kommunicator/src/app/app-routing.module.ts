import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CategorySidebarComponent } from './components/category-sidebar/category-sidebar.component';
import { MessageTemplateComponent } from './components/message-template/message-template.component';
import { ResponseTemplateComponent } from './components/response-template/response-template.component';
import { AdminMainPageComponent } from './pages/admin-main-page/admin-main-page.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { LogInPageComponent } from './pages/log-in-page/log-in-page.component';
import { ManageTopicsComponent } from './pages/manage-topics/manage-topics.component';
import { PersonalSubscriptionsComponent } from './pages/personal-subscriptions/personal-subscriptions.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';
import { UserSubscriptionPageComponent } from './pages/user-subscription-page/user-subscription-page.component';
import { SavedMessageTemplatesComponent } from './components/saved-message-templates/saved-message-templates.component';
import { SelectedMessageTemplateComponent } from './components/selected-message-template/selected-message-template.component';
import { AdminReportComponent } from './pages/admin-report/admin-report.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { OutgoingSetComponent } from './pages/outgoing-set/outgoing-set/outgoing-set.component';
import { DropDownOfCreatedMessagesComponent } from './components/drop-down-of-created-messages/drop-down-of-created-messages.component';
import { CreateTemplateComponent } from './pages/create-template/create-template.component';
import { MessageTemplateSidebarComponent } from './components/message-template-sidebar/message-template-sidebar.component';
import { MessageTemplatesComponent } from './pages/message-templates/message-templates.component';
import { ExperimentalShowTemplatesComponent } from './pages/experimental-show-templates/experimental-show-templates.component';
import { CreateResponseTemplateComponent } from './pages/create-response-template/create-response-template.component';
import { CreateMessageFlowComponent } from './pages/create-message-flow/create-message-flow.component';
import { CreateMessageTemplateComponent } from './components/create-message-template/create-message-template.component';
import { ResponsePageComponent } from './pages/response-page/response-page.component';
import { OutgoingSetTopicSubscriptionsComponent } from './components/outgoing-set-topic-subscriptions/outgoing-set-topic-subscriptions.component';

const routes: Routes = [
  { path: 'admin-main-page', component: AdminMainPageComponent, canActivate: [AuthenticationGuard] },
  { path: 'manage-topics', component: ManageTopicsComponent, canActivate: [AuthenticationGuard] },
  { path: 'response-template', component: ResponseTemplateComponent, canActivate: [AuthenticationGuard] },
  { path: 'user-subscription', component: UserSubscriptionPageComponent },
  { path: 'personal-subscriptions', component: PersonalSubscriptionsComponent },
  { path: 'subscribe', component: SubscribeComponent },
  { path: 'savedTemplates', component: SavedMessageTemplatesComponent, canActivate: [AuthenticationGuard] },
  { path: 'selectedTemplate', component: SelectedMessageTemplateComponent, canActivate: [AuthenticationGuard] },
  { path: 'outgoing-sets', component: OutgoingSetComponent, canActivate: [AuthenticationGuard] },
  { path: 'reports-admin', component: AdminReportComponent, canActivate: [AuthenticationGuard] },
  { path: 'dropdown', component: DropDownOfCreatedMessagesComponent, canActivate: [AuthenticationGuard] },
  { path: 'create-template', component: CreateTemplateComponent, canActivate: [AuthenticationGuard] },
  { path: 'messageTemplate-sidebar', component: MessageTemplateSidebarComponent, canActivate: [AuthenticationGuard] },
  { path: 'create-response-template', component: CreateResponseTemplateComponent, canActivate: [AuthenticationGuard] },
  { path: 'likert-response-page/:data', component: ResponsePageComponent, data: { likertResponse: true, textResponse: false } },
  { path: 'text-response-page/:data', component: ResponsePageComponent, data: { textResponse: true, likertResponse: false } },
  {path: 'create-message-flow', component: CreateMessageFlowComponent, canActivate: [AuthenticationGuard]},
  {path: 'create-message-template', component: CreateMessageTemplateComponent, canActivate: [AuthenticationGuard]},
  {path: 'outgoing-set-topic', component: OutgoingSetTopicSubscriptionsComponent},
  {path: 'login', component: LogInPageComponent},
  {
    path: '',
    pathMatch: 'full',
    component: LandingPageComponent
  },
  {path: '**' , redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }