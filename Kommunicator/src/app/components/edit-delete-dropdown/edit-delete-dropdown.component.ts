import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/models/category.model';
import { SubCategory } from 'src/models/subcategory.model';
import { Topic } from 'src/models/topic.model';

@Component({
  selector: 'app-edit-delete-dropdown',
  templateUrl: './edit-delete-dropdown.component.html',
  styleUrls: ['./edit-delete-dropdown.component.scss']
})
export class EditDeleteDropdownComponent implements OnInit {

  @Input() isCategory?: boolean;
  @Input() isSubCategory?: boolean;
  @Input() isTopic?: boolean;
  @Input() categoryId?: number;
  @Input() subCategoryId?: number;
  @Input() topicId?: number;

  @Output() editCategoryClick?: EventEmitter<any> = new EventEmitter<number>();
  @Output() editSubCategoryClick?: EventEmitter<any> = new EventEmitter<SubCategory>();
  @Output() editTopicClick?: EventEmitter<any> = new EventEmitter<Topic>();

  @Output() deleteCategoryClick?: EventEmitter<any> = new EventEmitter<number>();
  @Output() deleteSubCategoryClick?: EventEmitter<any> = new EventEmitter<SubCategory>();
  @Output() deleteTopicClick?: EventEmitter<any> = new EventEmitter<Topic>();

  constructor() { }

  ngOnInit(): void {
  }

  onEditClicked() {
    if (this.isSubCategory) {
      const subCategory: SubCategory = {
        id: this.subCategoryId,
        categoryID: this.categoryId,
        name: ""
      };
      this.editSubCategoryClick.emit(subCategory);
    } else if (this.isCategory){
      this.editCategoryClick.emit(this.categoryId);
    } else if (this.isTopic) {
      
      this.editTopicClick.emit();
    }
  }

  onDeleteClicked() {
    if (this.isSubCategory) {
      const subCategory: SubCategory = {
        id: this.subCategoryId,
        categoryID: this.categoryId,
        name: ""
      };
      this.deleteSubCategoryClick.emit(subCategory);
    } else if (this.isCategory) {
      this.deleteCategoryClick.emit(this.categoryId);
    } else if (this.isTopic) {
      
      this.deleteTopicClick.emit();
    }
  }
}
