import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() active: boolean;

  @Output() closeRequest = new EventEmitter();

  // Modal Header
  @Input() hasHeader: boolean;
  @Input() headerTitle: string = "Modal title2";

  // Modal Exit Button
  @Input() canClose: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  close() {
    this.closeRequest.emit();
  }
}
