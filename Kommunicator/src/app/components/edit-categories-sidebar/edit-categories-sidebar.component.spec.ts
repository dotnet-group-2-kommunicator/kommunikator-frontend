import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCategoriesSidebarComponent } from './edit-categories-sidebar.component';

describe('EditCategoriesSidebarComponent', () => {
  let component: EditCategoriesSidebarComponent;
  let fixture: ComponentFixture<EditCategoriesSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCategoriesSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCategoriesSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
