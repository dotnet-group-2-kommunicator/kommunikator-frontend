import { Component, Input, OnInit } from '@angular/core';
import { Category } from 'src/models/category.model';

@Component({
  selector: 'app-edit-categories-sidebar',
  templateUrl: './edit-categories-sidebar.component.html',
  styleUrls: ['./edit-categories-sidebar.component.scss']
})
export class EditCategoriesSidebarComponent implements OnInit {

  @Input() categories?: Category[];
  @Input() editable?: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
