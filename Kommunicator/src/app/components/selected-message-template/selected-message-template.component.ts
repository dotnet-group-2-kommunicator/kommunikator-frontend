import { Component, OnInit } from '@angular/core';
import { MessageTemplateService } from '../../services/message-template/message-template.service';
import {environment} from '../../../environments/environment';
@Component({
  selector: 'app-selected-message-template',
  templateUrl: './selected-message-template.component.html',
  styleUrls: ['./selected-message-template.component.scss']
})
export class SelectedMessageTemplateComponent implements OnInit {

  constructor(private messageTemplateService:MessageTemplateService) { }

  API_KEY_MCE=environment.API_KEY_TINYMCE;
  selectedMessage:any;
  dataModel:any;
 

  ngOnInit(): void {
    //this.selectedMessage=this.messageTemplateService.getOutgoingTemplateData();
    this.dataModel=this.selectedMessage.outerHTML;
    //this.selectedMessage=this.messageTemplateService.getOutgoingTemplateData();
  }
  sendOutEmails(){
    console.log(this.dataModel);


  }
  goToLibrary(){

  }

}
