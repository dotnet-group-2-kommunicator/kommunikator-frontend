import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedMessageTemplateComponent } from './selected-message-template.component';

describe('SelectedMessageTemplateComponent', () => {
  let component: SelectedMessageTemplateComponent;
  let fixture: ComponentFixture<SelectedMessageTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectedMessageTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedMessageTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
