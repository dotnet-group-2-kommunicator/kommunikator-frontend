import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedMessageTemplatesComponent } from './saved-message-templates.component';

describe('SavedMessageTemplatesComponent', () => {
  let component: SavedMessageTemplatesComponent;
  let fixture: ComponentFixture<SavedMessageTemplatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavedMessageTemplatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedMessageTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
