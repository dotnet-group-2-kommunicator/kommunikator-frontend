import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category/category.service';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';
import { TopicService } from 'src/app/services/topic/topic.service';
import { environment } from 'src/environments/environment';
import { Message } from 'src/models/message';
import { MessageTemplate } from 'src/models/MessageTemplate';
import { responseTemplate } from 'src/models/responseTemplate';

@Component({
  selector: 'app-create-message-template',
  templateUrl: './create-message-template.component.html',
  styleUrls: ['./create-message-template.component.scss']
})
export class CreateMessageTemplateComponent implements OnInit {

  //API key to use the TINY MCE
  API_KEY_MCE=environment.API_KEY_TINYMCE;
 
  dataModel:any="helloworld";
  index:any;

  //Model for loading the datamodel
  @Input() Content:string = "hello";
  @Input()TemplateName: string = "hello";

  @Input() Message?: MessageTemplate;

  @Output() MessageTemplateId = new EventEmitter();

  constructor(private MessageService: MessageTemplateService) { }

  ngOnInit(): void {
    this.MessageService.curentDataModel.subscribe(message => this.TemplateName = message)
    this.MessageService.curentContentDataModel.subscribe(message => this.Content = message)
  }

  receiveMessage(event) {
    this.Message = event;
    this.MessageService.changeDataModel(event.name)
    this.MessageService.changeContentDataModel(event.content)

    this.MessageTemplateId.emit(event.id);
  }

  async saveMessageTemplate() {
    return new Promise(resolve => {
      this.Message.content = this.Content;
      this.Message.name = this.TemplateName;

      this.MessageService.SaveMessageTemplate(this.Message).subscribe(user => {
        this.MessageTemplateId.emit(user.id);
        resolve();
      });
    })
  }

  deleteMessageTemplate() {
    let res = this.Message
    console.log("this is the res before deleting", res)
    this.MessageService.deleteMessage(res).subscribe((res) => {
      console.log('working delete')
    })
    
  
    
  }

  changeTemplateName(event) {
    this.TemplateName = event.target.value;
  }

}
