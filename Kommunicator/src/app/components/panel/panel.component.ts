import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {
  @Input() title: string = "Panel heading";
  @Input() searchPlaceholder: string = "Search";
  @Input() dataset: any;
  @Input() key: string;

  @Output() onItemSelected: EventEmitter<any> = new EventEmitter<any>();

  activeTab: number = 0;
  tabs = ["Selection", "Selected"];

  selected = [];

  constructor() { }

  ngOnInit(): void {
    console.log(`Type of key: ${typeof this.key}`);
  }

  changeTabs(index: number) {
    if (this.activeTab === index) {
      return;
    }

    this.activeTab = index;
  }

  removeSelected(item: any) {
    if (this.selected.includes(item)) {
      let index = this.selected.indexOf(item);
      this.selected.splice(index, 1);
    }
  }

  toggleSelected(item: any) {
    if (this.selected.includes(item)) {
      let index = this.selected.indexOf(item);

      this.selected.splice(index, 1);
    } else {
      this.selected.push(item);
    }

    //let items = ;
    let selectedItems = this.dataset.filter((_, index) => this.selected.includes(index));
    
    this.onItemSelected.emit(selectedItems);
  }
}
