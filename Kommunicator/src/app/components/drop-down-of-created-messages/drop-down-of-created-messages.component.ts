import { Component, OnInit } from '@angular/core';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';

@Component({
  selector: 'app-drop-down-of-created-messages',
  templateUrl: './drop-down-of-created-messages.component.html',
  styleUrls: ['./drop-down-of-created-messages.component.scss']
})
export class DropDownOfCreatedMessagesComponent implements OnInit {

  savedTemplateHeader: any;

  constructor(private messageService: MessageTemplateService) { }

  ngOnInit(): void {
    
  }
  getValuesFromDatabase(){
    this.messageService.getMessageTemplates().subscribe(
      x=>{
        console.log(x);
        this.savedTemplateHeader=x.map(
          x=>x.inputType
        );
      },
      err=>{
        console.log(err)
      }
    )  }

}
