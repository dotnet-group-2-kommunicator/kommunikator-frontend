import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropDownOfCreatedMessagesComponent } from './drop-down-of-created-messages.component';

describe('DropDownOfCreatedMessagesComponent', () => {
  let component: DropDownOfCreatedMessagesComponent;
  let fixture: ComponentFixture<DropDownOfCreatedMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DropDownOfCreatedMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropDownOfCreatedMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
