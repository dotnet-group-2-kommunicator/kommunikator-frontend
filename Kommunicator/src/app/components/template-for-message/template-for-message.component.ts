import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';
import { TopicsService } from 'src/app/services/topics/topics.service';
import { environment } from 'src/environments/environment';
import { ResponseTemplateSidebarComponent } from '../response-template-sidebar/response-template-sidebar.component';

@Component({
  selector: 'app-template-for-message',
  templateUrl: './template-for-message.component.html',
  styleUrls: ['./template-for-message.component.scss']
})
export class TemplateForMessageComponent implements OnInit {
//API key to use the TINY MCE
  API_KEY_MCE=environment.API_KEY_TINYMCE;

  //Model for loading the datamodel
  dataModel:any;
  titleOfTemplate:any;

  private subscription:Subscription;

  constructor(private topicService: TopicsService, private messageTemplateService:MessageTemplateService,
    private router:Router) {}


   ngOnInit(): void {
    this.titleOfTemplate="Template";
     //this.subscription=this.messageTemplateService.currentModelForMessageTemplate.subscribe(message=>{
       //this.dataModel=message.messageContent;
      //});
     this.subscription= this.messageTemplateService.curentDataModel.subscribe(message=>{
       console.log(message);
        this.titleOfTemplate=message.messageTemplate;
      })  
     }
  
     saveTemplate(){
      console.log(this.titleOfTemplate,this.dataModel)
    }
     ngOnDestroy(){
       this.subscription.unsubscribe();
     }
}


