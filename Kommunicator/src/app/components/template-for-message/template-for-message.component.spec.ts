import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateForMessageComponent } from './template-for-message.component';

describe('TemplateForMessageComponent', () => {
  let component: TemplateForMessageComponent;
  let fixture: ComponentFixture<TemplateForMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemplateForMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateForMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
