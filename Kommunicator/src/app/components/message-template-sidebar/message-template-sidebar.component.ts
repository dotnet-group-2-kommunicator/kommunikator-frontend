import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CategoryService } from 'src/app/services/category/category.service';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { Message } from 'src/models/message';
import { MessageTemplate } from 'src/models/MessageTemplate';

@Component({
  selector: 'app-message-template-sidebar',
  templateUrl: './message-template-sidebar.component.html',
  styleUrls: ['./message-template-sidebar.component.scss']
})
export class MessageTemplateSidebarComponent implements OnInit {

  @Output() SidebarDataMessage = new EventEmitter<any>();

  titleOfMessageTemplate: any;

  @Input() Messages?: MessageTemplate[];

  editable=false;

  constructor(private MessageService: MessageTemplateService, private messageService: MessageTemplateService) { }

  ngOnInit(): void {
    this.messageService.GetAllMessages().subscribe(res => {
      this.Messages = res;
    })
    }
    
    displayMessage(variabel){
        this.messageService.changeDataForMessageTemplate(variabel);
        this.messageService.changeDataModel(variabel);
    }
  


SendButtonData(data){
  console.log(data)
  this.SidebarDataMessage.emit(data);
  this.messageService.changeDataModel(data.name)
  this.messageService.changeContentDataModel(data.content)
}
}
