import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-action-card',
  templateUrl: './action-card.component.html',
  styleUrls: ['./action-card.component.scss']
})
export class ActionCardComponent implements OnInit {

  @Input() icon: string = "";
  @Input() title: string = "";
  @Input() navigation: string = "";

  @Input() clickable: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
