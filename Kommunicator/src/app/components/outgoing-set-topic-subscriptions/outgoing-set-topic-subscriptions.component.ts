import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Category } from 'src/models/category.model';
import { CategoryService } from 'src/app/services/category/category.service';
import { TopicService } from 'src/app/services/topic/topic.service';

import { Topic } from 'src/models/topic.model';
import { TopicSubscription } from 'src/models/TopicSubscription.model';
import { TopicSubscriptionService } from 'src/app/services/topic-subscription/topic-subscription.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-outgoing-set-topic-subscriptions',
  templateUrl: './outgoing-set-topic-subscriptions.component.html',
  styleUrls: ['./outgoing-set-topic-subscriptions.component.scss']
})
export class OutgoingSetTopicSubscriptionsComponent implements OnInit {
  categories: Category[] = [];
  topics: Topic[] = []

  searchText;
  categorySearch;
  subCategorySearch;
  message: string;
  allSubscriptions;

  emailList: string[] = [];

  @Output() onTopicSelected = new EventEmitter();
  @Output() OnEmailAdded = new EventEmitter();

  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.categoryService.fetchAllCategories().subscribe((res) => {
      this.categories = res;
    })
  }

  receiveMessage($event) {
    this.subCategorySearch = $event
  }

  receiveAllTopics($event) {
    this.categorySearch = ''
    this.subCategorySearch = ''
    this.searchText = ''
  }


  checkSubscribed(topic): boolean {
    return this.topics.includes(topic);
  }

  outgoingTopicList(topic: Topic) {
    if (this.topics.includes(topic)) {
      let index = this.topics.indexOf(topic);
      this.topics.splice(index, 1);
    } else {
      this.topics.push(topic);
    }

    // Create a distinct list of subscriber emails
    this.distinctEmailParser();
    this.onTopicSelected.emit(this.topics);
  }

  distinctEmailParser() {
    this.emailList = [];
    
    this.topics.forEach(topic => {
      topic.topicSubscriptions.forEach(ts => {
        if (ts.userEmail != undefined && !this.emailList.includes(ts.userEmail)) {
          this.emailList.push(ts.userEmail);
        }
      })
    });

    this.OnEmailAdded.emit(this.emailList);
  }
}
