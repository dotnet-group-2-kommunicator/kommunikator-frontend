import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgoingSetTopicSubscriptionsComponent } from './outgoing-set-topic-subscriptions.component';

describe('OutgoingSetTopicSubscriptionsComponent', () => {
  let component: OutgoingSetTopicSubscriptionsComponent;
  let fixture: ComponentFixture<OutgoingSetTopicSubscriptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutgoingSetTopicSubscriptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgoingSetTopicSubscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
