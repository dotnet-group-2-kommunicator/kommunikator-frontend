import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponseTemplateSidebarComponent } from './response-template-sidebar.component';

describe('ResponseTemplateSidebarComponent', () => {
  let component: ResponseTemplateSidebarComponent;
  let fixture: ComponentFixture<ResponseTemplateSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponseTemplateSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseTemplateSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
