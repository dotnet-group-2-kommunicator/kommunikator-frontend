import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';
import { Message } from 'src/models/message';
import { responseTemplate } from 'src/models/responseTemplate';

@Component({
  selector: 'app-response-template-sidebar',
  templateUrl: './response-template-sidebar.component.html',
  styleUrls: ['./response-template-sidebar.component.scss']
})
export class ResponseTemplateSidebarComponent implements OnInit {

  @Output() SidebarData = new EventEmitter<any>();



  @Input() editable?: boolean;

  @Input() Messages?: responseTemplate[];

  constructor(private MessageService: MessageTemplateService, private responseService:ResponseTemplateService) { }

  ngOnInit(): void {
    this.responseService.getAllResponseTemplates().subscribe(res => {
      this.Messages = res;
    })
  }

  SendButtonData(data){
    console.log("this is the data the button sends",data)
    this.SidebarData.emit(data);
    this.responseService.changeDataModel(data.name)
    this.responseService.changeContentDataModel(data.content)
  }


}
