import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/models/user.model';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private backend:BackendService) { }

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>(environment.API_GET_USERS);
  }
  
  postUsers(user:User):Observable<User>{
    return this.backend.post<User>('api/Users',user);
  }
}
