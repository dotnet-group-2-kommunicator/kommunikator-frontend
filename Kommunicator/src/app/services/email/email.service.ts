import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { EmailMessage } from 'src/models/emailMessage.model';
import { User } from 'src/models/user.model';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class EmailService {
  //Henter administratorer fra API-et kun for eksemplel. Egentlig skal brukere hentes, men har ikke lagt inn de i API-et enda
  private usersUrl = 'https://kommunicator.azurewebsites.net/api/users/';  
  private sendEmails='https://kommunicator.azurewebsites.net/api/email/sendemailstouser';

  private currentUser = new BehaviorSubject<string>(sessionStorage.getItem("userEmail"));
  currentUserModel = this.currentUser.asObservable();

  changeLoginEmail(email: string) {
    sessionStorage.setItem("userEmail", email);
    this.currentUser.next(email);
  }

  addedSubscribers:any[];

  constructor(private backend: BackendService) {}

    getUsers(): Observable<any[]> {
      return this.backend.get<any[]>(this.usersUrl);
    }

    doesUserExist(email: string) : Observable<boolean> {
      return this.backend.get<boolean>(`api/users/exists/${email}`);
    }

    registerUser(email: string) {
      return this.backend.post<User>(`api/users`, {
        UserEmail: email,
        Active: true
      });
    }

     sendEmailToMultipleUsers():Observable<any[]>{   
      return this.backend.get<any[]>(this.sendEmails);
    }

    deliverEmail(email: EmailMessage) {
      return this.backend.post(`api/email`, email);
    }
}
