import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TopicSubscription, TopicSubscriptions } from 'src/models/TopicSubscription.model';

@Injectable({
  providedIn: 'root'
})
export class TopicSubscriptionService {

  constructor(private http: HttpClient) {  }

  fetchAllSubscriptions(id: string): Observable<TopicSubscriptions> {
    return this.http.get<TopicSubscriptions>(`https://kommunicator.azurewebsites.net/api/Users/subscriptions/${id}`)
  }

  updateActiveFlag(id: number, email: string): Observable<TopicSubscription> {
    return this.http.get<TopicSubscription>(`https://kommunicator.azurewebsites.net/api/TopicSubscription/${id}/${email}`);
  }

  

  subscribeTopic(id: number, user: string): Observable<TopicSubscription> {
    return this.http.post<TopicSubscription>('https://kommunicator.azurewebsites.net/api/TopicSubscriptions2', {
      TopicID: id,
      active: true,
      userEmail: user
    })
  }




}
