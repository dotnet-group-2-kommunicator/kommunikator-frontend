import { TestBed } from '@angular/core/testing';

import { TopicSubscriptionService } from './topic-subscription.service';

describe('TopicSubscriptionService', () => {
  let service: TopicSubscriptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopicSubscriptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
