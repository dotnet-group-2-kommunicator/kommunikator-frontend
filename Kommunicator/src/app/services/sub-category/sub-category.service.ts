import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SubCategory } from 'src/models/subcategory.model';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class SubCategoryService {

  constructor(private backend: BackendService) { }

  addsubCategory(name) : Observable<SubCategory> {
    return this.backend.post<SubCategory>("api/SubCategories", name);
  }

  addsubCategory2( name): Observable<SubCategory> {
    return this.backend.post<SubCategory>('api/SubCategories', name);
  }

  newSubCategory(newSubCat: SubCategory): Observable<SubCategory> {
    return this.backend.post<SubCategory>('api/SubCategories', newSubCat);
  }

  editSubCategory(subCategory: SubCategory) : Observable<SubCategory> {
    
    return this.backend.put<SubCategory>('api/SubCategories/' + subCategory.id, subCategory);
  }

  deleteSubCategory(subCategory: SubCategory) {
    return this.backend.put('api/SubCategories/delete/' + subCategory.id, subCategory);
  }
}
