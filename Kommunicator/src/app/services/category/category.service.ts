import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Category } from 'src/models/category.model';
import { Observable } from 'rxjs';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  constructor(private http: BackendService) { }

  fetchAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>("api/Categories/sub-categories/topics");
  }

  newCategory(category: Category) : Observable<Category> {
    return this.http.post<Category>('api/Categories', category);
  }

  editCategory(category: Category) : Observable<Category> {
    return this.http.put<Category>('api/Categories/' + category.id, category);
  }

  deleteCategory(categoryId: number) {
    return this.http.put('api/Categories/delete/' + categoryId, {});
  }
}
