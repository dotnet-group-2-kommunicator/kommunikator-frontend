import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { KeycloakService} from 'keycloak-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { KeycloakAccessToken, KeycloakRestLogin } from 'src/models/keycloak.model';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  constructor(private http: HttpClient, private keycloak: KeycloakService, private router: Router) {
    console.log("AuthService initialized");
  }

  async authenticated() : Promise<boolean> {
    let state: boolean = await this.keycloak.isLoggedIn();
    return state;
  }

  async GetToken() : Promise<string> {
    return await this.keycloak.getToken();
  }

  async GetEmail() {
    return await this.keycloak.getUsername();
  }

  authenticate() {
    //this.keycloak.login()
  }

  async Logout(redirectUri: string = "/landingpage") {
    return await this.keycloak.logout(window.location.origin + redirectUri);
  }

  getAdminAccessToken(endpoint: string = "") : Observable<KeycloakAccessToken> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    let pload: URLSearchParams = new URLSearchParams();
    pload.set('username', environment.KEYCLOAK_REST_USER);
    pload.set('password', environment.KEYCLOAK_REST_PASS);
    pload.set('client_secret', environment.KEYCLOAK_REST_SECRET);
    pload.set('grant_type', "password");
    pload.set('client_id', 'admin-cli');

    let payload = pload.toString();

    //console.log(pload.toString())

    return this.http.post<KeycloakAccessToken>(`${environment.KEYCLOAK_REST}realms/master/protocol/openid-connect/token`, payload, {
      headers: headers
    });
    //return this.http.get
  }


}
