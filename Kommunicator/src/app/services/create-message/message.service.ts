import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Message } from '../../../models/message';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: BackendService) { }

  public createMessage(message: Message) {
    return this.http.post(`api/Messages`, {
      messageTemplateID: message.messageTemplateID,
      responseTemplateID: message.responseTemplateID,
      messageDescription: message.messageDescription,
      active: true
    });
  }

  public fetchAllMessages() : Observable<Message[]> {
    return this.http.get<Message[]>(`api/Messages`);
  }
  public fetchSpecificMessage(id):Observable<Message>{
    return this.http.get<Message>(`https://kommunicator.azurewebsites.net/api/Messages/${id}`)
  }
}
