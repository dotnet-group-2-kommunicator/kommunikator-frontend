import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { BackendService } from '../backend.service';
import { Response } from 'src/models/response.model';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  constructor(private http: HttpClient, private backend: BackendService) { }

  addResponse(response: Response) : Observable<Response> {
    return this.backend.post<Response>('api/Responses', response);
  }
  getResponse():Observable<Response[]>{
    return this.backend.get<Response[]>('api/Responses');
  }

}
