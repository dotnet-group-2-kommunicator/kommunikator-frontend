export interface MessageTemplate {
    id?: number,
    name?: string,
    content?: string

}