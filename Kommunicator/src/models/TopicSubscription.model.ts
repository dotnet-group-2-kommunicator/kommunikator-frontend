import { Topic } from './topic.model';
import { User } from './user.model';

export interface TopicSubscriptions {
    topicSubscriptions: TopicSubscription[],
    userEmail: string
}

export interface TopicSubscription {
    topicID?: number,
    userEmail?: string,
    active?: boolean,
    user?: User,
    topicSubscriptions?: Topic[]
}