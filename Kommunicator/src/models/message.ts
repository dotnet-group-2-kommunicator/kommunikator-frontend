import { MessageTemplate } from './MessageTemplate';
import { responseTemplate } from './responseTemplate';

export interface Message {
    messageID?: number,
    messageTemplateID?: number,
    messageTemplate?: MessageTemplate,
    responseTemplateID?: number,
    responseTemplate?: responseTemplate,
    messageDescription?: string
}