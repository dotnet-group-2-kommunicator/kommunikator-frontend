import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { User } from './user.model';

export interface Response {
    id?: number,
    responseContent?: string,
    userEmail: string,
    user?: User,
    messageId: number,
    Message?: Message
    active?: boolean
}