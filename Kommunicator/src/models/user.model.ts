import { TopicSubscription } from './TopicSubscription.model';

export interface User {
    userEmail: string,
    active?: boolean,
    topicSubscritions?: TopicSubscription[]
}