export interface responseTemplate {
    responseTemplateID?: number,
    name?: string,
    content?: string,
    responseType?: number,
}