// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_KEY_TINYMCE: "txiytlrj70tqnmraxnpyudnzfwm0w2m9xy3wc7qox55c0t7l",
  API_GET_USERS: "https://kommunicator.azurewebsites.net/api/users",
  API_GET_TOPICS:"https://kommunicator.azurewebsites.net/api/topics",
  API_GET_COMPONENTS:"https://kommunicator.azurewebsites.net/api/Components",
  API_POST_COMPONENTS:"https://kommunicator.azurewebsites.net/api/Components",
  API_GET_CATEGORIES:"https://kommunicator.azurewebsites.net/api/Categories",
  API_GET_MESSAGES: "",

  // Keycloak REST API Credentials
  KEYCLOAK_REST: "https://auth-kommunicator.azurewebsites.net/auth/",
  KEYCLOAK_REST_SECRET: "1fd5f482-c1c2-48fe-af39-b5ff77fc2611",
  KEYCLOAK_REST_USER: "group2",
  KEYCLOAK_REST_PASS: "adming2",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
